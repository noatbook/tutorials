# Debian relay-only MTA (nullmailer)

## TOC

1. The Enigma
2. Application of the Arcane Secrets
3. Fundamental Axioms
4. The Ritual
  4.1 Gmail Account Configuration
  4.2 The /etc/nullmailer/remotes File
  4.3 The /etc/nullmailer/defaultdomain File
  4.4 The /etc/nullmailer/adminaddr File
  4.5 Testing
  4.6 Troubleshooting
5. The Forbidden Sources
6. Lifespan

## The Enigma

This morning while cleaning up the lab after a series of failed alchemical
experiments, I accidentally dropped the trusty grimoire and it opened up on the
shifting incantations. I feel like the Great Old Ones must be trying to tell me
something, so I shall share my experience with these incantations.

Here we will explore one ethereal beast summoned for the sole purpose of
honoring the edicts set by the Wise Ones, we will call this beast the Mail
Transfer Agent (MTA), its duty is to contribute to the greater good of the
Simple Mail Transfer Protocol (SMTP).

Here I will share with you how to summon the beast and tame it to make it do
your willing and send all those emails coming from your esoteric artifacts, or
servers, as the commoners like to call them, yuck, to the outer world (wide
web).

## Application of the Arcane Secrets

You have probably noticed that linux (or unix or \*nix, I don't know how far
down the rabbit hole I should go) machines are very fond of sending emails to
notify someone about some important things and it makes sense because most
linux machines are actually servers and we are not amused by having to manually
monitor everything repeatedly.

I have read that some distributions are configured to be able to send emails
externally out-of-the-box (and I am still wondering how) but that was not my
experience. With this knowledge you will be able to charm your artifact to make
it communicate with the outer world, to give it a voice you can hear, without
going to /var/mail each time.

# Fundamental Axioms

We must understand SMTP before touching its building blocks, so, here it is,
from the Necronomicon itself:

[SMTP overview](./assets/smtp.png)

SMTP diagram

Very basically, the protocol follows this order:

1. The sender, a mere mortal, uses a Mail User Agent (MUA) to send an email to
   the receiver, that is another mere mortal with no grasp of the supernatural.
   The MUA is a program that is used to manage email (create, read, send,
   etc.), some examples are Outlook and Thunderbird. The MUA sends the email to
   the Mail Submission Agent (MSA) which performs some checks (beyond the scope
   of the Necronomicon) on the emails before handing them to the Mail Transfer
   Agent (MTA) which is in charge of getting the email to the receiver. Often,
   the MSA and the MTA inhabit the same cursed vessel.
2. The MTA then has to ask a Domain Name System (DNS) server where the Mail
   eXchanger (MX) server is located, because it has some questions to ask. The
   DNS server is akin to an address book for the forsaken corners of the
   internet, serving an eternal covenant of locating stuff beyond this realm.
3. After locating MX's tainted habitat, MTA will ask it how to send the email
   so it can travel safe to the receiver. MX's duty is to know where an email
   is headed just by inspecting it and how to get there (approximately, because
   there may or may not be a direct route).
4. The MTA is ready to trust MX's route to the receiver and send the email on a
   journey filled with yet unknown threats. On its journey, the email may go
   directly to the target MTA on the receiving side or make multiple shorter
   treks to the target. The email may be escorted by friendly entities that
   agree to relay it to the next ally until it reaches the target MTA.
5. The target MTA will then send the message to the Mail Delivery Agent (MDA),
   this one is in charge of delivering email locally to mailboxes. The email
   has finished its perilous journey and is now safely awaiting retrieval.
6. The receiving end will then retrieve the message from the mailbox using the
   Internet Message Access Protocol (IMAP) or the Post Office Protocol (POP) to
   finally give it to the receiving user.
