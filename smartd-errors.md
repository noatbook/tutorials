# S.M.A.R.T troubleshooting

## Pending sectors

One day I got an email alert about the Current_Pending_Sector count rising,
which is considered to be an indication of disk failure, this is the output of
`smartctl` on said disk, which marks the test as `PASSED` however there were
some errors

### `smartctl -a /dev/sdb` after a long selftest

```
root@lilchewchew:~# smartctl -a /dev/sdb
smartctl 6.6 2017-11-05 r4594 [x86_64-linux-4.19.0-16-amd64] (local build)
Copyright (C) 2002-17, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Seagate Barracuda 3.5
Device Model:     ST4000DM005-2DP166
Serial Number:    WDH2JSHZ
LU WWN Device Id: 5 000c50 0a93d9094
Firmware Version: 0001
User Capacity:    4,000,787,030,016 bytes [4.00 TB]
Sector Sizes:     512 bytes logical, 4096 bytes physical
Rotation Rate:    5980 rpm
Form Factor:      3.5 inches
Device is:        In smartctl database [for details use: -P show]
ATA Version is:   ACS-3 T13/2161-D revision 5
SATA Version is:  SATA 3.1, 6.0 Gb/s (current: 3.0 Gb/s)
Local Time is:    Tue Apr  6 17:46:29 2021 CDT
SMART support is: Available - device has SMART capability.
SMART support is: Enabled

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

General SMART Values:
Offline data collection status:  (0x82) Offline data collection activity
                                        was completed without error.
                                        Auto Offline Data Collection: Enabled.
Self-test execution status:      ( 249) Self-test routine in progress...
                                        90% of test remaining.
Total time to complete Offline
data collection:                (  581) seconds.
Offline data collection
capabilities:                    (0x7b) SMART execute Offline immediate.
                                        Auto Offline data collection on/off support.
                                        Suspend Offline collection upon new
                                        command.
                                        Offline surface scan supported.
                                        Self-test supported.
                                        Conveyance Self-test supported.
                                        Selective Self-test supported.
SMART capabilities:            (0x0003) Saves SMART data before entering
                                        power-saving mode.
                                        Supports SMART auto save timer.
Error logging capability:        (0x01) Error logging supported.
Short self-test routine
recommended polling time:        (   1) minutes.
Extended self-test routine
recommended polling time:        ( 628) minutes.
Conveyance self-test routine
recommended polling time:        (   2) minutes.
SCT capabilities:              (0x10a5) SCT Status supported.
                                        SCT Data Table supported.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x000f   083   064   006    Pre-fail  Always       -       184985728
  3 Spin_Up_Time            0x0003   094   094   000    Pre-fail  Always       -       0
  4 Start_Stop_Count        0x0032   100   100   020    Old_age   Always       -       50
  5 Reallocated_Sector_Ct   0x0033   100   100   010    Pre-fail  Always       -       0
  7 Seek_Error_Rate         0x000f   089   060   045    Pre-fail  Always       -       713411558
  9 Power_On_Hours          0x0032   069   069   000    Old_age   Always       -       27459 (139 179 0)
 10 Spin_Retry_Count        0x0013   100   100   097    Pre-fail  Always       -       0
 12 Power_Cycle_Count       0x0032   100   100   020    Old_age   Always       -       160
183 Runtime_Bad_Block       0x0032   100   100   000    Old_age   Always       -       0
184 End-to-End_Error        0x0032   100   100   099    Old_age   Always       -       0
187 Reported_Uncorrect      0x0032   100   100   000    Old_age   Always       -       0
188 Command_Timeout         0x0032   100   100   000    Old_age   Always       -       0 0 0
189 High_Fly_Writes         0x003a   100   100   000    Old_age   Always       -       0
190 Airflow_Temperature_Cel 0x0022   066   046   040    Old_age   Always       -       34 (Min/Max 29/38)
191 G-Sense_Error_Rate      0x0032   100   100   000    Old_age   Always       -       0
192 Power-Off_Retract_Count 0x0032   100   100   000    Old_age   Always       -       1196
193 Load_Cycle_Count        0x0032   100   100   000    Old_age   Always       -       1651
194 Temperature_Celsius     0x0022   034   054   000    Old_age   Always       -       34 (0 19 0 0 0)
197 Current_Pending_Sector  0x0012   100   100   000    Old_age   Always       -       8
198 Offline_Uncorrectable   0x0010   100   100   000    Old_age   Offline      -       8
199 UDMA_CRC_Error_Count    0x003e   200   200   000    Old_age   Always       -       0
240 Head_Flying_Hours       0x0000   100   253   000    Old_age   Offline      -       27226h+59m+58.110s
241 Total_LBAs_Written      0x0000   100   253   000    Old_age   Offline      -       27845159953
242 Total_LBAs_Read         0x0000   100   253   000    Old_age   Offline      -       396357132903

SMART Error Log Version: 1
No Errors Logged

SMART Self-test log structure revision number 1
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Extended offline    Self-test routine in progress 90%     27459         -
# 2  Extended offline    Completed: read failure       10%     27398         -
# 3  Extended offline    Completed: read failure       10%     27355         -
# 4  Extended offline    Completed: read failure       10%     27306         -
# 5  Extended offline    Completed: read failure       10%     27258         -
# 6  Extended offline    Completed without error       00%     27185         -
# 7  Extended offline    Completed without error       00%     27137         -
# 8  Extended offline    Completed without error       00%     27089         -
# 9  Extended offline    Completed without error       00%     27017         -
#10  Extended offline    Completed without error       00%     26969         -
#11  Extended offline    Completed without error       00%     26922         -
#12  Extended offline    Completed without error       00%     26856         -
#13  Extended offline    Completed without error       00%     26815         -
#14  Extended offline    Completed without error       00%     26690         -
#15  Extended offline    Completed without error       00%     26642         -
#16  Extended offline    Completed without error       00%     26595         -
#17  Extended offline    Completed without error       00%     26522         -
#18  Extended offline    Completed without error       00%     26475         -
#19  Extended offline    Completed without error       00%     26425         -
#20  Extended offline    Completed without error       00%     26357         -
#21  Extended offline    Completed without error       00%     26305         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.Short self-test routine
recommended polling time:        (   1) minutes.
Extended self-test routine
recommended polling time:        ( 628) minutes.
Conveyance self-test routine
recommended polling time:        (   2) minutes.
SCT capabilities:              (0x10a5) SCT Status supported.
                                        SCT Data Table supported.

SMART Attributes Data Structure revision number: 10
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
  1 Raw_Read_Error_Rate     0x000f   083   064   006    Pre-fail  Always       -       184985728
  3 Spin_Up_Time            0x0003   094   094   000    Pre-fail  Always       -       0
  4 Start_Stop_Count        0x0032   100   100   020    Old_age   Always       -       50
  5 Reallocated_Sector_Ct   0x0033   100   100   010    Pre-fail  Always       -       0
  7 Seek_Error_Rate         0x000f   089   060   045    Pre-fail  Always       -       713411558
  9 Power_On_Hours          0x0032   069   069   000    Old_age   Always       -       27459 (139 179 0)
 10 Spin_Retry_Count        0x0013   100   100   097    Pre-fail  Always       -       0
 12 Power_Cycle_Count       0x0032   100   100   020    Old_age   Always       -       160
183 Runtime_Bad_Block       0x0032   100   100   000    Old_age   Always       -       0
184 End-to-End_Error        0x0032   100   100   099    Old_age   Always       -       0
187 Reported_Uncorrect      0x0032   100   100   000    Old_age   Always       -       0
188 Command_Timeout         0x0032   100   100   000    Old_age   Always       -       0 0 0
189 High_Fly_Writes         0x003a   100   100   000    Old_age   Always       -       0
190 Airflow_Temperature_Cel 0x0022   066   046   040    Old_age   Always       -       34 (Min/Max 29/38)
191 G-Sense_Error_Rate      0x0032   100   100   000    Old_age   Always       -       0
192 Power-Off_Retract_Count 0x0032   100   100   000    Old_age   Always       -       1196
193 Load_Cycle_Count        0x0032   100   100   000    Old_age   Always       -       1651
194 Temperature_Celsius     0x0022   034   054   000    Old_age   Always       -       34 (0 19 0 0 0)
197 Current_Pending_Sector  0x0012   100   100   000    Old_age   Always       -       8
198 Offline_Uncorrectable   0x0010   100   100   000    Old_age   Offline      -       8
199 UDMA_CRC_Error_Count    0x003e   200   200   000    Old_age   Always       -       0
240 Head_Flying_Hours       0x0000   100   253   000    Old_age   Offline      -       27226h+59m+58.110s
241 Total_LBAs_Written      0x0000   100   253   000    Old_age   Offline      -       27845159953
242 Total_LBAs_Read         0x0000   100   253   000    Old_age   Offline      -       396357132903

SMART Error Log Version: 1
No Errors Logged

SMART Self-test log structure revision number 1
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Extended offline    Self-test routine in progress 90%     27459         -
# 2  Extended offline    Completed: read failure       10%     27398         -
# 3  Extended offline    Completed: read failure       10%     27355         -
# 4  Extended offline    Completed: read failure       10%     27306         -
# 5  Extended offline    Completed: read failure       10%     27258         -
# 6  Extended offline    Completed without error       00%     27185         -
# 7  Extended offline    Completed without error       00%     27137         -
# 8  Extended offline    Completed without error       00%     27089         -
# 9  Extended offline    Completed without error       00%     27017         -
#10  Extended offline    Completed without error       00%     26969         -
#11  Extended offline    Completed without error       00%     26922         -
#12  Extended offline    Completed without error       00%     26856         -
#13  Extended offline    Completed without error       00%     26815         -
#14  Extended offline    Completed without error       00%     26690         -
#15  Extended offline    Completed without error       00%     26642         -
#16  Extended offline    Completed without error       00%     26595         -
#17  Extended offline    Completed without error       00%     26522         -
#18  Extended offline    Completed without error       00%     26475         -
#19  Extended offline    Completed without error       00%     26425         -
#20  Extended offline    Completed without error       00%     26357         -
#21  Extended offline    Completed without error       00%     26305         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.
```

Apparently, since this is a Seagate drive, these numbers might mean nothing,
the use of `seatools` is recommended in order to diagnose these kinds of issues

### `badblocks -nvs /dev/sdb`
