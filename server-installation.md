# Initial configuration for a new server

## Add contrib and non-free sections to /etc/apt/sources.list

## Minimal installation packages

- Drivers specific to architecture (like gpu card)
  - Install nvidia-detect and follow instructions
- Editor => vim, neovim
- SSH server => openssh
- Utilities => rsync
- Unattended upgrades => unattended-upgrades apt-listchanges (and configure)

## Install basic packages

- ZFS => See ZFS installation tutorial
- SAMBA => samba
- ACL => acl
- Firewall => ufw
- Mail relay agent => nullmailer, see installation tutorial
- SMART checking => smartd

## Server hardening

????
