# Setup

## Required packages

- Install linux-headers-\`uname -r\`
- Add `contrib` section to install zfsutils-linux
- Run `apt update`
- Install `zfsutils-linux`
- Install `samba`
- Install `acl` (optional)
- Install `ntfs-3g`

An error like this might appear:

```
Job for zfs-mount.service failed because the control process exited with error code.
See "systemctl status zfs-mount.service" and "journalctl -xe" for details.
invoke-rc.d: initscript zfs-mount, action "start" failed.
● zfs-mount.service - Mount ZFS filesystems
   Loaded: loaded (/lib/systemd/system/zfs-mount.service; enabled; vendor preset: enabled)
   Active: failed (Result: exit-code) since Mon 2021-02-22 18:58:05 CST; 5ms ago
     Docs: man:zfs(8)
  Process: 123079 ExecStart=/sbin/zfs mount -a (code=exited, status=1/FAILURE)
 Main PID: 123079 (code=exited, status=1/FAILURE)

Feb 22 18:58:05 debian-minimal systemd[1]: Starting Mount ZFS filesystems...
Feb 22 18:58:05 debian-minimal zfs[123079]: The ZFS modules are not loaded.
Feb 22 18:58:05 debian-minimal zfs[123079]: Try running '/sbin/modprobe zfs' as root to load them.
Feb 22 18:58:05 debian-minimal systemd[1]: zfs-mount.service: Main process exited, code=exited, status=1/FAILURE
Feb 22 18:58:05 debian-minimal systemd[1]: zfs-mount.service: Failed with result 'exit-code'.
Feb 22 18:58:05 debian-minimal systemd[1]: Failed to start Mount ZFS filesystems.
dpkg: error processing package zfsutils-linux (--configure):
 installed zfsutils-linux package post-installation script subprocess returned error exit status 1
Setting up g++ (4:8.3.0-1) ...
update-alternatives: using /usr/bin/g++ to provide /usr/bin/c++ (c++) in auto mode
Setting up build-essential (12.6) ...
dpkg: dependency problems prevent configuration of zfs-zed:
 zfs-zed depends on zfsutils-linux (>= 0.7.12-2+deb10u2); however:
  Package zfsutils-linux is not configured yet.

dpkg: error processing package zfs-zed (--configure):
 dependency problems - leaving unconfigured
Processing triggers for systemd (241-7~deb10u6) ...
Processing triggers for man-db (2.8.5-2) ...
Processing triggers for libc-bin (2.28-10) ...
Errors were encountered while processing:
 zfsutils-linux
 zfs-zed
E: Sub-process /usr/bin/dpkg returned an error code (1)
root@debian-minimal:~# 
```

Which is fixed by running `modprobe zfs`, installation can be resumed by `apt upgrade`.

## Install new disks (VM only)

For the purpose of this tutorial, everything is tested on a VM, so new disks
need to be installed to work on

## Calculate correct size for disks

ZFS needs to use disks with the same sizes, using different sized disks will
end up wasting space. Using different sized disks usually leads to an error but
you can force the operation and have ZFS waste the extra space it cannot use.
The following example was created with 2x 2GB and 1x 4GB.

```
root@debian-minimal:~# zfs list
NAME              USED  AVAIL  REFER  MOUNTPOINT
vault-dev         135K  3.83G  30.6K  /vault-dev
vault-dev/vault  30.6K  3.83G  30.6K  /vault-dev/vault
```

Technically, we have 8GB of available space, which equals about 2.6GB * 2
available space plus 2.6GB parity, however that exceeds the capacity of our 2GB
disks and is thus impossible.

This makes it so we're forced to use same size disks so space doesn't go to
waste, this is achieved by partitioning all disks to be as big as the smallest.

## Partition your disks properly!

It might be necessary to execute `wipefs` on your device to have a clean device
to work with

## Creating your pool or virtual device (VDEV)

In order to create the pool you need to get a list of devices that you want to
add to the pool, we usually use UIDs because the /dev/ device naming might
change between restarts.

In order to list your devices, you can query the following path:

```
root@debian-minimal:~# ls -lt /dev/disk/by-path/
total 0
lrwxrwxrwx 1 root root 10 Mar  4 19:03 pci-0000:00:10.0-scsi-0:0:3:0-part1 -> ../../sdd1
lrwxrwxrwx 1 root root 10 Mar  4 19:03 pci-0000:00:10.0-scsi-0:0:2:0-part1 -> ../../sdc1
lrwxrwxrwx 1 root root 10 Mar  4 19:03 pci-0000:00:10.0-scsi-0:0:2:0-part9 -> ../../sdc9
lrwxrwxrwx 1 root root  9 Mar  4 19:03 pci-0000:00:10.0-scsi-0:0:2:0 -> ../../sdc
lrwxrwxrwx 1 root root 10 Mar  4 19:03 pci-0000:00:10.0-scsi-0:0:1:0-part1 -> ../../sdb1
lrwxrwxrwx 1 root root 10 Mar  4 19:03 pci-0000:00:10.0-scsi-0:0:1:0-part9 -> ../../sdb9
lrwxrwxrwx 1 root root  9 Mar  4 19:03 pci-0000:00:10.0-scsi-0:0:1:0 -> ../../sdb
lrwxrwxrwx 1 root root 10 Mar  4 18:50 pci-0000:00:10.0-scsi-0:0:0:0-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Mar  4 18:50 pci-0000:00:10.0-scsi-0:0:3:0-part2 -> ../../sdd2
lrwxrwxrwx 1 root root 10 Mar  4 18:50 pci-0000:00:10.0-scsi-0:0:0:0-part5 -> ../../sda5
lrwxrwxrwx 1 root root 10 Mar  4 18:50 pci-0000:00:10.0-scsi-0:0:0:0-part2 -> ../../sda2
lrwxrwxrwx 1 root root  9 Mar  4 18:50 pci-0000:00:07.1-ata-2 -> ../../sr0
lrwxrwxrwx 1 root root  9 Mar  4 18:50 pci-0000:00:10.0-scsi-0:0:0:0 -> ../../sda
lrwxrwxrwx 1 root root  9 Mar  4 18:50 pci-0000:00:10.0-scsi-0:0:3:0 -> ../../sdd
```

IMPORTANT: it is highly recommended to set ashift=12 on pool creation when the
disks being used have a sector size of 4069 bytes (or 4KiB) because the size
reported by the disk is often inaccurate and is much lower than it actually is
(512 bytes reportes when the real value is 4096 bytes)

And then you build your create command like so:

`zpool create -o ashift=12 vol raidz pci-0000:00:10.0-scsi-0:0:1:0 pci-0000:00:10.0-scsi-0:0:2:0 pci-0000:00:10.0-scsi-0:0:3:0-part1`

Which will create a VDEV called `vol`, it will use raidz and will composed of three "disks"

RAIDZ uses three "disks", two of them are used for data and the third one is
used for parity. The information is not actually distributed like that, but the
amount of space used does correspond to that

I say "disks" because in reality you can use any kind of device you like, in my
case, there are only two full disks and the third device is actually a
partition of a disk, this is done because my third disk is larger than the
others and the extra space would be wasted

## Provisioning your file system or volume (ZVOL)

IMPORTANT

If you intend to share your pool through SMB, you need to enable
casesensitivity=mixed

When creating a ZVOL, you only have to specify options and the name of the target, like so:

`zfs create -o casesensitivity=mixed vol/fs1`

`casesensitivity` is one of the only options that need to be set at creation
time, if you don't you get a message like this:

```
root@debian-minimal:~# zfs set casesensitivity=mixed vol/fs1
cannot set property for 'vol/fs1': 'casesensitivity' is readonly
```

After creating the ZVOL, we can proceed with tweaking it to our liking

## Setting properties on the ZVOL

Here's where you'll want to configure your ZVOL taking into account the kind of
use you plan for it

## Create users and groups for SAMBA

1. You want to create a group to which your samba share will belong
2. Create users that will interact with the share, add the users to the group from step 1
3. Create the users for samba (samba uses a user list separated from the unix one)

## Configure permissions on the folder to be shared

### Change ownership of the folder

The folder should be owned by root, but the group should be set to our samba
group, this will allow all of our designated users to modify the share,
change the ownership recursively if you have files in the share already

### Change permissions of the folder

We want to setup the permissions of this folder to be 770, this provides full
control to the owner and the group, everybody else is denied

### Setup an ACL

This is an optional step, samba should handle all access correctly, however, if
you're also touching the folder with unix users, permissions and access might
be messed up, the ACL will help us standardize all access through unix while
samba handles all the foreign connections

### setgid bit

When all ownership, permission and ACL settings have been done, you will want
to set the setgid bit on the parent folder, this makes it so every operation on
the folder is performed as the group that owns the folder, thus, evertyhing
inside is owned by the group that owns the parent folder

This is the last step in order to standardize access to the shared folder when
accessed by different means

### Configure the samba share

Configure the share to your liking

### Setup automatic monthly scrubbing

1. Create a script that can send emails in case of scrubbing failure

Sample script `/usr/local/bin/systemd-email`:

```
#!/bin/sh

/usr/bin/mail -s "Service status: $1" root <<ERRMAIL

$(systemctl status --full "$1")
ERRMAIL
```

2. Create a service that calls the email script

Sample script: `/etc/systemd/system/status_email_user@.service`:

```
[Unit]
Description=status email for %i to user

[Service]
Type=oneshot
ExecStart=/usr/local/bin/systemd-email %i
User=nobody
Group=systemd-journal
```

3. Create a timer service that will trigger monthly (recommended scrubbing periodicity)

Sample script: `/etc/systemd/system/zfs-scrub@.timer`

```
[Unit]
Description=Monthly zpool scrub on %i

[Timer]
OnCalendar=monthly
AccuracySec=1h
Persistent=true

[Install]
WantedBy=multi-user.target
```

4. Create a service that is triggered by the timer (same name as the timer)

Sample script: `/etc/systemd/system/zfs-scrub@.service`

```
[Unit]
Description=zpool scrub on %i
OnFailure=status_email_user@%n.service

[Service]
Nice=19
IOSchedulingClass=idle
KillSignal=SIGINT
ExecStart=/usr/sbin/zpool scrub %i

[Install]
WantedBy=multi-user.target
```

5. Enable and start the service, use the pool name as parameter

`systemctl enable zfs-scrub@pool-to-scrub.timer`
`systemctl start zfs-scrub@pool-to-scrub.timer`
`systemctl enable zfs-scrub@pool-to-scrub`
`systemctl start zfs-scrub@pool-to-scrub`

# Testing the ZFS/SMB/ACL setup on a VM

Speeds are way beyond what they normally are, after two tests with 1GB and 3GB packet sizes we got interesting results

![1GB speedtest](assets/2021-02-12_lan_speed_test.png)

![3GB speedtest](assets/2021-02-12_20_38_59-LAN_Speed_Test.png)

Second installation

![](assets/2nd speed test.png)

Final installation on PROD server

![](assets/lst_20210315_after_upgrade.png)
